# coding=utf-8

from flask import Flask, render_template
from flask import jsonify, request, session 
import requests
from datetime import datetime
import datetime

import json

app = Flask(__name__) 

@app.route('/Assignment2')
def Assignment2():
    return render_template('Assignment2.html')


@app.route('/fin_info_calc', methods=['POST'])
def fin_info_calc():
    if request.method == 'POST':
        stock_symbol = ''
        stock_name = ''
        stock_price = ''
        symbol = request.form['symbol'].strip()
        api_key = 'CX68ZXZG9PWG8UDQ'

        request_name = requests.get("https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords=" + symbol +
                              "&apikey=" + api_key)


        nm = request_name.json()

        if nm['bestMatches']:
            full_name = nm['bestMatches']
            if full_name:
                for full in full_name:
                    if '1.0000' in full['9. matchScore']:
                        stock_name = full['2. name']
            else:
                stock_name = "Check if Symbol Entered is Correct"

         
            r_value = requests.get("https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=" + symbol +
                        "&apikey=" + api_key)
            d = r_value.json()

            if 'Global Quote' in d:
                out = d['Global Quote']
                if out:
                    stock_symbol = "(" + out['01. symbol'] + ")"
                    stock_price = out['05. price'] + " " + out['09. change'] + " (" + out['10. change percent'] + ")"
                else:
                    stock_name = "Check if Stock symbol Entered is Correct"
            elif 'Note' in d:
                stock_price = "Limitation from API Source: Limit 5 calls per minute and 500 calls per day"

            else:
                stock_price = "Error in Capturing Data for this stock symbol. Try Again."
        else:
            stock_name = "Check if stock symbol Entered is Correct"

        # set the system date
        now = datetime.datetime.now()
        output_dt = now.strftime("%c") + " " + "PST"


        result = {'output_symbol': stock_symbol, 'output_value': stock_price, 'output_name': stock_name,
                  'output_dt': output_dt}

        return render_template('Assignment2.html', result=result)

@app.route('/')
def index():
   return render_template('index.html')

finalResult = {}
@app.route('/result',methods = ['POST'])
def getPrice():
    input= json.dumps(request.json)
    data = input
    print(data)
    allotment = float(request.json['allotment'])
    finalPrice = float(request.json['finalPrice'])
    sellCom = float(request.json['sellCom'])
    buyCom = float(request.json['buyCom'])
    initialPrice = float(request.json['initialPrice'])
    tax = float(request.json['gainPerc'])
    
    proceeds = allotment * finalPrice
    finalResult["allotment"] = "{0:,.2f}".format(allotment);
    finalResult["finalPrice"] = "{0:,.2f}".format(finalPrice);
    finalResult["sellCom"] = "{0:,.2f}".format(sellCom);
    finalResult["buyCom"] = "{0:,.2f}".format(buyCom);
    finalResult["initialPrice"] = "{0:,.2f}".format(initialPrice);
    finalResult["initialPrice"] = "{0:,.2f}".format(initialPrice);
    finalResult["tax"] = "{0:,.2f}".format(tax);
    finalResult['gainPerc'] = "{0:,.2f}".format(tax);

    finalResult["proceeds"] = "{0:,.2f}".format(proceeds)
    finalResult["totalPurchasePrice"] = allotment * initialPrice
    finalResult["gain"] = proceeds - finalResult["totalPurchasePrice"] - sellCom - buyCom
    finalResult["totalTax"] = finalResult["gain"]*tax/100
    cost = finalResult["totalPurchasePrice"] + sellCom + buyCom + finalResult["totalTax"]
    finalResult["cost"] = "{0:,.2f}".format(cost)
    netProfit = proceeds - cost
    finalResult["netProfit"] = "{0:,.2f}".format(netProfit)
    finalResult["returnIn"] = "{0:,.2f}".format( 100*netProfit/cost )
    finalResult["brkEvn"] = "{0:,.2f}".format( (finalResult["totalPurchasePrice"] + buyCom +sellCom)/allotment)

  #  finalResult["comm"] = "{0:,.2f}".format(finalResult["proceeds"]-(finalResult["totalPurchasePrice"]+ finalResult["buyCom"]+ (float)finalResult["sellCom"]))
    jsonResult = json.dumps(finalResult)


    return jsonResult

if __name__ == "__main__" :
    app.run( host='0.0.0.0',port = 80) 
